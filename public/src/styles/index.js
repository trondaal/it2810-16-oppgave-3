var styles = {
        jumbo: {
            paddingLeft: '10px',
            paddingTop: '10px !important',
        },
        h1: {
            textAlign: 'center',
            marginBottom: '30px',
            color: 'white'
        },
        h3Grid: {
            height: '100%',
            margin: '0',
            overflow: 'hidden'
        },
        input: {
            width: '80%',
            marginLeft: '10%',
            padding: '12px 12px',
            marginTop: '15px',
            borderRadius: '10px',
            border: '2px solid #6c6c7f'
        },
        frontPage: {
            background: '#fff',
            paddingTop: '10px',
            top: '-17px',
            boxShadow: '2px 2px 2px #888'
        },
        profilePictureStyle: {
            color: 'white',
            fontSize: '22px',
            marginTop: '12px'
        },
        profileText: {
            color: 'white',
            fontSize: '18px',
            marginLeft: '7px',
            marginTop: '10px',
        },
        h3Filter: {
            display: 'inline',
            float: 'left',
            width: '10%',
            marginBottom: '0px'
        },
        divFilter: {
            display: 'inline'
        },
        borderTop: {
            borderTop: '2px solid #d4d4d4',
            paddingTop: '10px'
        },
        marginTop: {
            marginTop: '5px'
        },
        grid: {
            width: '100%',
            paddingRight: '0px',
            paddingLeft: '0px'
        },
        toggleButton: {
            position: 'absolute',
            top: '236px',
            right: '0px'
        },
        loadMoreButton: {
            width: '20%',
            marginLeft: '40%',
            height: '44px',
            fontSize: '20px',
            marginBottom: '20px'
        },
        loadWrapper: {
            width: '80px',
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        smallLoader: {
            width: '80px'
        },
        largeLoadWrapper: {
            width: '140px',
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        favoriteList: {
            marginRight: '15px',
            marginLeft: '15px'
        },
        logOutButton: {
            marginLeft: '15px'
        },
        tagsWrapper: {
            marginLeft: '20px',
        },
    }
    ;

module.exports = styles;