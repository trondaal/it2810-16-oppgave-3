var React = require('react');
var NavBar = require('./NavBar');
var PropTypes = React.PropTypes;
var styles = require('../styles');
var Col = require('react-bootstrap').Col;

// FrontPage renders the navigation bar and wraps around the other components (see router.js)
function FrontPage(props) {
    return (
        <div>
            <NavBar isLoggedIn={props.isLoggedIn}/>
            <Col style={styles.frontPage} sm={10} md={10} lg={10} smOffset={1} mdOffset={1} lgOffset={1}>
                {props.child}
            </Col>
        </div>
    )
}

FrontPage.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
};

module.exports = FrontPage;