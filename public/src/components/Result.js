var React = require('react');
var PropTypes = React.PropTypes;
var styles = require('../styles');
var DrinkTable = require('./table/DrinkTable');
var Button = require('react-bootstrap').Button;
var DrinkGrid = require('./drinkgrid/DrinkGrid');

// Decides if the tableview or the gridview will show depending on if the button has been clicked
function Result(props) {
    var List = props.toggle ? DrinkGrid : DrinkTable;
    return (
        <div style={styles.borderTop}>
            <Button style={styles.toggleButton} onClick={props.view}>Toggle View </Button>
            <List
                drinks={props.drinks}
                setDisplayedResults={props.setDisplayedResults}/>
            {props.loading || !props.initialLoaded ?
                <div style={styles.loadWrapper}>
                    <img style={styles.smallLoader} src="../../images/load.svg"/>
                </div> : null}
        </div>
    );
}

Result.propTypes = {
    onNextPage: PropTypes.func.isRequired,
    drinks: PropTypes.array.isRequired,
};

module.exports = Result;
