var React = require('react');
var PropTypes = React.PropTypes;
var styles = require('../styles');
var Filter = require('./table/Filter');
var Result = require('../components/Result');
var Jumbotron = require('react-bootstrap').Jumbotron;

// Renders the main content of the page and passes props down to the classes using them
function MainContent(props) {
    return (
        <div style={{position: 'relative'}}>
            <Jumbotron style={{
                backgroundImage: 'url("../../images/IMG_0346r.jpg")',
                backgroundSize: '100% 100%',
                paddingTop: '10px',
                paddingBottom: '30px',
                marginBottom: '0px',
                borderRadius: '10px'
            }}>
                <div>
                    <h1 style={styles.h1}>Search for Drinks</h1>
                    <input
                        className="searchInput"
                        style={styles.input}
                        type="text"
                        placeholder="drinks/name"
                        onChange={props.onUpdateSearch}
                        value={props.search}/>
                </div>
            </Jumbotron>

            <Filter
                setColorFilter={props.setColorFilter}
                setTasteFilter={props.setTasteFilter}
                applyFilters={props.applyFilters}
            />
            <Result
                view={props.view}
                toggle={props.toggle}
                loading={props.loading}
                initialLoaded={props.initialLoaded}
                setDisplayedResults={props.setDisplayedResults}
                drinks={props.displayedResults}
                results={props.results}
                onNextPage={props.onNextPage}/>
        </div>
    )
}

// The expected props sendt from MainContentContainer and what type they are expected to be.
MainContent.propTypes = {
    onUpdateSearch: PropTypes.func.isRequired,
    results: PropTypes.array.isRequired,
    search: PropTypes.string.isRequired,
    counter: PropTypes.number.isRequired,
    displayedResults: PropTypes.array.isRequired,
    onNextPage: PropTypes.func.isRequired
};

module.exports = MainContent;
