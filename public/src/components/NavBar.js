var React = require('react');
var styles = require('../styles');
var Link = require('react-router').Link;
var Nav = require('react-bootstrap').Nav;
var Navbar = require('react-bootstrap').Navbar;
var GlyphIcon = require('react-bootstrap').Glyphicon;

// Renders the navigation bar on the top of the page
var NavBar = React.createClass({
    render(){
        return (
            <Navbar inverse fixedTop collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to="/">Drinks</Link>
                    </Navbar.Brand>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav pullRight>
                        { this.props.isLoggedIn ?
                            <div>
                                <Link to="/user">
                                    <GlyphIcon glyph="user" style={styles.profilePictureStyle}/>
                                    <span style={styles.profileText}>My Profile</span>
                                </Link>
                                <a style={styles.logOutButton} href="/users/logout">Log out</a>
                            </div>
                            :
                            <a href="/auth/google">
                                <img src="../../images/google/btn_google_signin.png"/>
                            </a>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }
});

module.exports = NavBar;
