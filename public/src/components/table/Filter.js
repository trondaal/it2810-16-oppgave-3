var React = require('react');
var Row = require('react-bootstrap').Row;
var Col = require('react-bootstrap').Col;
var styles = require('../../styles/index');
var MenuItem = require('react-bootstrap').MenuItem;
var DropdownButton = require('react-bootstrap').DropdownButton;


var Filter = React.createClass({

    // The inital state of this component
    getInitialState: function () {
        return {
            currentColor: 'Color',
            currentTaste: 'Taste'
        }
    },

    // Eventhandler for the dropdown menu to filter by color
    changeColor(event){
        if (event !== 'None') {
            this.props.setColorFilter(true, event);
            this.setState({
                currentColor: event
            });
        } else {
            this.props.setColorFilter(false, event);
            this.setState({
                currentColor: 'Color'
            });
        }
    },

    // Eventhandler for the dropdown menu to filter by taste
    changeTaste(event){

        if (event !== 'None') {
            this.props.setTasteFilter(true, event);
            this.setState({
                currentTaste: event
            });
        } else {
            this.props.setTasteFilter(false, event);
            this.setState({
                currentTaste: 'Taste'
            });
        }
    },

    // Function to render the dropdown menus for colors and taste
    render: function () {
        return (
            <div>
                <h3 style={styles.h3Filter}> Filters </h3>
                <Row style={styles.divFilter}>
                    <Col xs={4} sm={2} md={2} lg={2}>
                        <DropdownButton id='currentColor' title={this.state.currentColor} onSelect={this.changeColor}>
                            <MenuItem eventKey="None">None</MenuItem>
                            <MenuItem eventKey="Red">Red</MenuItem>
                            <MenuItem eventKey="Pink">Pink</MenuItem>
                            <MenuItem eventKey="Yellow">Yellow</MenuItem>
                            <MenuItem eventKey="Brown">Brown</MenuItem>
                            <MenuItem eventKey="Blue">Blue</MenuItem>
                            <MenuItem eventKey="Green">Green</MenuItem>
                            <MenuItem eventKey="Purple">Purple</MenuItem>
                            <MenuItem eventKey="Transparent">Transparent</MenuItem>
                            <MenuItem eventKey="White">White</MenuItem>
                        </DropdownButton>
                    </Col>
                    <Col xs={4} sm={2} md={2} lg={2}>
                        <DropdownButton id='currentTaste' title={this.state.currentTaste} onSelect={this.changeTaste}>
                            <MenuItem eventKey="None">None</MenuItem>
                            <MenuItem eventKey="Bitter">Bitter</MenuItem>
                            <MenuItem eventKey="Fresh">Fresh</MenuItem>
                            <MenuItem eventKey="Fruity">Fruity</MenuItem>
                            <MenuItem eventKey="Herb">Herb</MenuItem>
                            <MenuItem eventKey="Spicy">Spicy</MenuItem>
                            <MenuItem eventKey="Sour">Sour</MenuItem>
                            <MenuItem eventKey="Sweet">Sweet</MenuItem>
                        </DropdownButton>
                    </Col>


                </Row>
            </div>

        )
    }
});

module.exports = Filter;
