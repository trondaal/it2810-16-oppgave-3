var React = require('react');
var Table = require('react-bootstrap').Table;
var DrinkTableRow = require('./DrinkTableRow');

var DrinkTable = React.createClass({

    // Function to sort the drinks by name in ascending order
    sortByNameAsc(){
        var drinks = this.props.drinks;
        this.props.setDisplayedResults(
            drinks.sort(function (a, b) {
                return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0)
            })
        );
    },

    // Function to sort the drinks by name in descending order
    sortByNameDes() {
        var drinks = this.props.drinks;
        this.props.setDisplayedResults(
            drinks.sort(function (a, b) {
                return (a.name < b.name) ? 1 : ((b.name < a.name) ? -1 : 0)
            })
        );
    },

    // Function to sort the drinks by rating in ascending order
    sortByRatingAsc(){
        var drinks = this.props.drinks;
        this.props.setDisplayedResults(
            drinks.sort(function (a, b) {
                return (a.rating > b.rating) ? 1 : ((b.rating > a.rating) ? -1 : 0)
            })
        );
    },

    // Function to sort the drinks by rating in descending order
    sortByRatingDes() {
        var drinks = this.props.drinks;
        this.props.setDisplayedResults(
            drinks.sort(function (a, b) {
                return (a.rating < b.rating) ? 1 : ((b.rating < a.rating) ? -1 : 0)
            })
        );
    },

    // Function to sort the drinks by difficulty in ascending order
    sortBySkillAsc(){
        var drinks = this.props.drinks;
        this.props.setDisplayedResults(
            drinks.sort(function (a, b) {
                return (a.skill.value > b.skill.value) ? 1 : ((b.skill.value > a.skill.value) ? -1 : 0)
            })
        );
    },

    // Function to sort the drinks by name in descending order
    sortBySkillDes() {
        var drinks = this.props.drinks;
        this.props.setDisplayedResults(
            drinks.sort(function (a, b) {
                return (a.skill.value < b.skill.value) ? 1 : ((b.skill.value < a.skill.value) ? -1 : 0)
            })
        );
    },

    // Render function that renders the rows in the table. The rows are created
    // through DrinkTableRow with the props
    render() {
        const drinks = this.props.drinks;
        return (
            <Table striped hover>
                <thead>
                <tr>
                    <th>
                        Name &nbsp;
                        <span className="pointer" onClick={this.sortByNameAsc}>▲</span>
                        <span className="pointer" onClick={this.sortByNameDes}>▼</span>
                    </th>
                    <th>
                        Rating &nbsp;
                        <span className="pointer" onClick={this.sortByRatingAsc}>▲</span>
                        <span className="pointer" onClick={this.sortByRatingDes}>▼</span>
                    </th>
                    <th>
                        Difficulty &nbsp;
                        <span className="pointer" onClick={this.sortBySkillAsc}>▲</span>
                        <span className="pointer" onClick={this.sortBySkillDes}>▼</span>
                    </th>
                </tr>
                </thead>

                <tbody>
                {drinks.map(function (drink, i) {
                    return <DrinkTableRow key={i} drink={drink}/>
                })}
                </tbody>


            </Table>
        )
    }
});

module.exports = DrinkTable;
