var React = require('react');
var DrinkModal = require('../modal/DrinkModal');

var DrinkTableRow = React.createClass({

    // Set initial state variable "showExpandedView" to false
    getInitialState() {
        return {showExpandedView: false};
    },

    // Changes the state to show the expanded view
    showExpandedView(){
        this.setState({showExpandedView: true});
    },

    // Changes the state to close the expanded view
    closeExpandedView() {
        this.setState({showExpandedView: false});
    },

    // Saves the props to a constant and returns a row in the gridview
    render() {
        const drink = this.props.drink;
        return (
            <tr className="pointer" onClick={this.showExpandedView}>
                <td>{drink.name}</td>
                <td>{drink.rating + '/100'}</td>
                <td>{drink.skill.name}</td>
                { this.state.showExpandedView ?
                    <DrinkModal
                        close={this.closeExpandedView}
                        drink={drink}/> : null}
            </tr>
        )
    }
});

module.exports = DrinkTableRow;
