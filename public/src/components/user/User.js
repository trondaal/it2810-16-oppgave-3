var React = require('react');
var PropTypes = React.PropTypes;
var Row = require('react-bootstrap').Row;
var Col = require('react-bootstrap').Col;
var HistoryList = require('./HistoryList');
var FavoriteList = require('./FavoriteList');
var Panel = require('react-bootstrap').Panel;
var ListGroup = require('react-bootstrap').ListGroup;
var ListGroupItem = require('react-bootstrap').ListGroupItem;

// Class to show the userpage with the user's favorites and history
var User = React.createClass({
    render() {

        if(this.props.isLoggedIn == null){
            return <div><img src="../../../images/load.svg"/></div>
        }

        if (this.props.isLoggedIn) {
            return (
                <div>
                    <h1> My profile </h1>
                    <Row>
                        <Col sm={6} md={6} lg={6}>
                            <Panel collapsible defaultExpanded header="Account Information">
                                <ListGroup fill>
                                    <ListGroupItem>
                                        <b>Name: </b> {this.props.userInfo.name}
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <b>Email: </b> {this.props.userInfo.email}
                                    </ListGroupItem>
                                </ListGroup>
                            </Panel>
                        </Col>
                        <Col sm={6} md={6} lg={6}>
                            <HistoryList
                                histories={this.props.histories}
                                historiesLoaded={this.props.historiesLoaded}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <FavoriteList
                                favorites={this.props.favorites}
                                favoritesLoaded={this.props.favoritesLoaded}/>
                        </Col>
                    </Row>
                </div>)
        } else {
            return <h1>You need to log in to see this page.</h1>
        }
    }
});

User.propTypes = {
    favorites: PropTypes.array.isRequired,
    histories: PropTypes.array.isRequired
};

module.exports = User;
