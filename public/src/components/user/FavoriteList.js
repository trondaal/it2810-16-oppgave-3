var React = require('react');
var styles = require('../../styles');
var Col = require('react-bootstrap').Col;
var Row = require('react-bootstrap').Row;
var Panel = require('react-bootstrap').Panel;
var DrinkListItem = require('./DrinkListItem');

var FavoriteList = React.createClass({
    render() {
        return (
            <Panel style={styles.favoriteList} header={"My Favorite Drinks"}>
                <Row>
                    {this.props.favoritesLoaded ?
                        <div>
                            {this.props.favorites.map(function (favorite, i) {
                                return (
                                    <Col key={i} md={2} lg={2} sm={5}>
                                        <DrinkListItem drink={favorite}/>
                                    </Col>
                                )
                            })}
                        </div>
                        :
                        <div style={styles.largeLoadWrapper}>
                            <img src="../../images/load.svg"/>
                        </div>
                    }
                </Row>
            </Panel>
        )
    }
});


module.exports = FavoriteList;
