var React = require('react');
var styles = require('../../styles/index');
var Panel = require('react-bootstrap').Panel;
var ListGroup = require('react-bootstrap').ListGroup;
var ListGroupItem = require('react-bootstrap').ListGroupItem;

var HistoryList = React.createClass({
    render() {
        return (
            <div>
                <Panel collapsible defaultExpanded header="Search History">
                {this.props.historiesLoaded ?

                    <ListGroup fill>
                        {this.props.histories.map(function (history, i) {
                            return (
                                <ListGroupItem key={i}>
                                    {history.drink_name}
                                </ListGroupItem>);
                        })}
                    </ListGroup>
                    :
                    <div style={styles.largeLoadWrapper}>
                         <img src="../../images/load.svg"/>
                    </div>
                }
                </Panel>
            </div>
        )
    }
});

module.exports = HistoryList;