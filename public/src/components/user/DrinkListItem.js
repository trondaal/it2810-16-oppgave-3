var React = require('react');
var Addb = require('../../config/addb');
var Col = require('react-bootstrap').Col;
var DrinkModal = require('./../modal/DrinkModal');
var Thumbnail = require('react-bootstrap').Thumbnail;

var DrinkListItem = React.createClass({

    getInitialState() {
        return {
            showExpandedView: false,
            show: true
        };
    },

    showExpandedView(){
        this.setState({showExpandedView: true});
    },

    closeExpandedView() {
        this.setState({showExpandedView: false});
    },

    hideComponenet(){
        this.setState({show: false})
    },

    render() {
        const drink = this.props.drink;

        return (

            <Col>
                {this.state.show ?
                    <Thumbnail style={{height: '130px'}} className='pointer' onClick={this.showExpandedView}
                               src={Addb.assetURL.drinks + '50x150/' + drink.id + '.png'}>
                        <p style={{textAlign: 'center'}}>{drink.name}</p>
                    </Thumbnail> : null
                }
                { this.state.showExpandedView ?
                    <DrinkModal
                        close={this.closeExpandedView}
                        hideListItem={this.hideComponenet}
                        fromFavoriteList={true}
                        drink={drink}/> : null}
            </Col>
        )
    }
});


module.exports = DrinkListItem;
