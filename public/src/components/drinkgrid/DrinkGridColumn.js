var React = require('react');
var styles = require('../../styles');
var Addb = require('../../config/addb');
var Col = require('react-bootstrap').Col;
var DrinkModal = require('../modal/DrinkModal');
var Thumbnail = require('react-bootstrap').Thumbnail;

var DrinkGridColumn = React.createClass({

    // Set initial state variable "showExpandedView" to false
    getInitialState() {
        return {showExpandedView: false,};
    },

    // Changes the state to show the expanded view
    showExpandedView(){
        this.setState({showExpandedView: true});
    },

    // Changes the state to close the expanded view
    closeExpandedView() {
        this.setState({showExpandedView: false});
    },
    render() {
        // Saves the props to a constant and returns a column in the gridview
        const drink = this.props.drink;
        return (
            <Col>
                <Thumbnail
                    style={{height: '426px'}}
                    className='pointer' src={Addb.assetURL.drinks + '250x350/' + drink.id + '.png'}
                    alt="242x200"
                    onClick={this.showExpandedView}>

                    <h3 style={styles.h3Grid}>{drink.name}</h3>
                    { this.state.showExpandedView ?
                        <DrinkModal
                            close={this.closeExpandedView}
                            drink={drink}/> : null}

                </Thumbnail>
            </Col>
        )
    }
});

module.exports = DrinkGridColumn;
