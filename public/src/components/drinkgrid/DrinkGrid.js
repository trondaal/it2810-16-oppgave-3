var React = require('react');
var styles = require('../../styles');
var Row = require('react-bootstrap').Row;
var Col = require('react-bootstrap').Col;
var Grid = require('react-bootstrap').Grid;
var DrinkGridColumn = require('./DrinkGridColumn');


var DrinkGrid = React.createClass({

    // The render function creates DrinkGridColumns from the props
    //  and adds them to the grid.
    render() {
        const drinks = this.props.drinks;
        return (
            <Grid style={styles.grid}>
                <Row>
                    {drinks.map(function (drink, i) {
                        return (
                            <Col key={i} md={4} lg={4} sm={10}>
                                <DrinkGridColumn drink={drink}/>
                            </Col>)
                    })}
                </Row>
            </Grid>
        )
    }
});

module.exports = DrinkGrid;
