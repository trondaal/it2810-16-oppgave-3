var React = require('react');
var styles = require('../../styles/index');
var Label = require('react-bootstrap').Label;

var Tags = React.createClass({

    // Class for displaying extended information about a drink in the form of tags
    getTags(){
        var tags = [];
        const drink = this.props.drink;
        for (var i = 0; i < drink.tastes.length; i++) {
            tags.push(drink.tastes[i].text)
        }
        for (var j = 0; j < drink.occasions.length; j++) {
            tags.push(drink.occasions[j].text)
        }
        if (drink.isCarbonated) {
            tags.push('Carbonated')
        }
        if (drink.isHot) {
            tags.push('Hot')
        }
        if (drink.isAlcoholic) {
            tags.push('Alcoholic')
        } else {
            tags.push('Non-Alcoholic')
        }
        return tags;
    },

    render: function () {
        var tags = this.getTags();
        return (
            <div style={styles.tagsWrapper}>
                {tags.map(function (tag, i) {
                    return <Label key={i} className="drinkTag" bsStyle="info">{tag}</Label>
                })}
            </div>
        )
    }
});

module.exports = Tags;
