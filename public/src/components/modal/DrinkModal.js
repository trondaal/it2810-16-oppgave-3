var $ = require('jquery');
var React = require('react');
var Tags = require('./Tags');
var Addb = require('../../config/addb');
var Modal = require('react-bootstrap').Modal;
var Button = require('react-bootstrap').Button;
var apiRoutes = require('../../config/apiRoutes');
var GlyphIcon = require('react-bootstrap').Glyphicon;

var DrinkModal = React.createClass({

    getInitialState() {
        return {imageLoaded: false, isFavorite: false, isLoggedIn: null};
    },

    componentDidMount() {
        var self = this;
        $.ajax({
            url: apiRoutes.favorites.isFavorite,
            type: 'post',
            dataType: 'json',
            data: {'drink': {'id': this.props.drink.id}}, //this.props.drink.id
            success: function (data) {
                self.setState({isFavorite: data, isLoggedIn: true})
            },
            error: function (error) {
                self.setState({isLoggedIn: false})
            }
        });

        $.ajax({
            url: apiRoutes.histories.add,
            type: 'post',
            dataType: 'json',
            data: {'drink': {'id': this.props.drink.id, 'name': this.props.drink.name}}, //this.props.drink.id
            success: function (data) {
            }
        });
    },

    addToFavorites() {
        // Async call in order to add a favorite.
        var self = this;
        $.ajax({
            url: apiRoutes.favorites.add,
            type: 'post',
            dataType: 'json',
            data: {'drink': {'id': this.props.drink.id}}, //this.props.drink.id
            success: function (data) {
                self.setState({isFavorite: true})
            }
        });
    },

    removeFromFavorites(){
        var self = this;
        if (this.props.fromFavoriteList) {
            this.props.hideListItem();
        }
        $.ajax({
            url: apiRoutes.favorites.delete,
            type: 'post',
            dataType: 'json',
            data: {'drink': {'id': this.props.drink.id}}, //this.props.drink.id
            success: function (data) {
                self.setState({isFavorite: false})
            }
        });
    },

    handleImageLoaded() {
        this.setState({imageLoaded: true});
    },

    render() {

        const drink = this.props.drink;
        const drinkContainer = {
            width: '600px',
            margin: 'auto'
        };

        const imgContainerStyle = {
            width: '300px',
            height: '400px',
            float: 'left'
        };
        const loaderStyle = {
            position: 'fixed',
            left: '100px',
            top: '250px'
        };

        const ingredientStyle = {
            paddingTop: '100px',
            width: '250px',
            height: '400px',
            float: 'left'

        };

        const boldText = {
            fontWeight: 'bold',
            fontSize: '16px'
        };

        const drinkName = {
            marginLeft: '20px'
        };

        const description = {
            fontWeight: 'bold',
            fontSize: '16px',
            marginLeft: '20px'
        };

        const descriptionText = {
            marginLeft: '20px',
            width: '90%'
        };

        return (
            <Modal show={true} onHide={this.props.close}>
                <Modal.Header closeButton></Modal.Header>

                <Modal.Body>
                    <div id="drinkContainer" style={drinkContainer}>
                        <h1 style={drinkName}>{drink.name}</h1>
                        <div id='DrinkModalImageContainer' style={imgContainerStyle}>
                            {!this.state.imageLoaded ? <img
                                src="../../../images/load.svg"
                                style={loaderStyle}/>
                                : null}
                            <img
                                src={Addb.assetURL.drinks + '300x400/' + drink.id + '.png'}
                                onLoad={this.handleImageLoaded}
                            />
                        </div>
                        <div id='ingredients' style={ingredientStyle}>
                            <p style={boldText}> Ingredients </p>
                            {drink.ingredients.map(function (ingredient, i) {
                                return <p key={i}> {ingredient.textPlain} </p>
                            })}
                        </div>
                        <div id="description">
                            <p style={description}>Description</p>
                            <p style={descriptionText}>{drink.descriptionPlain}</p>
                        </div>
                        <Tags drink={drink}/>
                    </div>
                </Modal.Body>

                <Modal.Footer>
                    {this.state.isLoggedIn == null ?
                        <div><img style={{width: '50px'}} src="../../../images/load.svg" /></div>
                        :
                        <div>
                            {this.state.isLoggedIn ?
                                <div>
                                    {this.state.isFavorite ?
                                        <Button onClick={this.removeFromFavorites} bsSize="large"> Unfavorite <GlyphIcon
                                            glyph="star"/> </Button>
                                        :
                                        <Button onClick={this.addToFavorites} bsSize="large"> Favorite <GlyphIcon
                                            glyph="star-empty"/> </Button>
                                    }
                                </div>
                                :
                                <div>
                                    <p>Sign in to add as favorite. <a href="/auth/google">
                                        <img src="../../../images/google/btn_google_signin.png" alt="googleSignIn"/>
                                    </a></p>

                                </div>
                            }
                        </div>
                    }
                </Modal.Footer>
            </Modal>
        )
    }
});

module.exports = DrinkModal;
