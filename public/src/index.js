/**
 * Created by sondre on 27/10/2016.
 */
var React = require('react');
var ReactDOM = require('react-dom');
var routes = require('./config/routes');
//import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
    routes,
    document.getElementById('app')
);