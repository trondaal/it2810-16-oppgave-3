var React = require('react');
var PropTypes = React.PropTypes;
var NavBar2 = require('../components/NavBar');

function FrontPage(props) {
    return (
        <div className="main-content">
            <NavBar2 loggedIn={props.isLoggedIn}/>
            {this.props.children}
        </div>
    )
}

FrontPage.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
};

module.exports = FrontPage;