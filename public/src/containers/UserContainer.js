var $ = require('jquery');
var React = require('react');
var User = require('../components/user/User');
var apiRoutes = require('../config/apiRoutes');

var UserContainer = React.createClass({
    getInitialState: function () {
        return {
            isLoggedIn: null,
            userInfo: [],
            favorites: [],
            histories: [],
            historiesLoaded: false,
            favoritesLoaded: false,
        }
    },

    componentDidMount: function () {
        // Async call in order to get user data
        $.get(apiRoutes.users.find, function (user) {
            this.setState(() => {
                return {
                    isLoggedIn: user instanceof Object,
                    userInfo: user,
                };
            });
        }.bind(this));

        $.get(apiRoutes.favorites.find, function (favorites) {
            this.setState(() => {
                return {
                    favorites: favorites,
                    favoritesLoaded: true
                };
            });
        }.bind(this));

        $.get(apiRoutes.histories.find, function (histories) {
            this.setState(() => {
                return {
                    histories: histories,
                    historiesLoaded: true
                };
            });
        }.bind(this));
    },

    render: function () {
        return (
            <User
                isLoggedIn={this.state.isLoggedIn}
                userInfo={this.state.userInfo}
                favorites={this.state.favorites}
                favoritesLoaded={this.state.favoritesLoaded}
                histories={this.state.histories}
                historiesLoaded={this.state.historiesLoaded}/>
        )
    }
});

module.exports = UserContainer;