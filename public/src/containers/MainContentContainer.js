var $ = require('jquery');
var React = require('react');
var addb = require('../config/addb');
var MainContent = require('../components/MainContent');

var MainContentContainer = React.createClass({

    // Initial state of this component
    getInitialState: function () {
        return {
            counter: 25,
            searchCounter: 25,
            results: [],
            searchText: '',
            displayedResults: [],
            unFilteredResults: [],
            colorFilter: {'active': false, color: 'None'},
            tasteFilter: {'active': false, taste: 'None'},
            toggle: false,
            loading: false,
            initialLoaded: false,
        }
    },

    // Eventhandler for toggling the tableview and gridview
    toggleview: function (e) {
        this.setState({toggle: this.state.toggle ? false : true})
    },

    // Eventhandler that updates the search when the user types in the searchbar
    handleUpdateSearch: function (event) {
        var self = this;
        $.ajax({
            type: "GET",
            url: addb.quickSearch + event.target.value + '/?apiKey=' + addb.apiKey + '&pageSize=25',
            dataType: "jsonp",
            success: function (data) {
                self.setState({
                    unFilteredResults: data.result,
                    displayedResults: data.result,
                    loading: false
                }, function () {
                    this.applyFilters();
                });
                window.addEventListener('scroll', this.handleScroll);
            },
        });
        this.setState({
            searchCounter: 25,
            searchText: event.target.value,
        });
    },

    // AJAX call to get data from API
    httpGetJSON: function (url) {
        var self = this;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "jsonp",
            success: (data) => {
                self.setState({
                    unFilteredResults: self.state.unFilteredResults.concat(data.result),
                    displayedResults: self.state.displayedResults.concat(data.result),
                    initalLoaded: true,
                    loading: false
                }, function () {
                    this.applyFilters();
                });
                window.addEventListener('scroll', this.handleScroll);
            },
        })
    },

    componentDidMount: function () {
        this.httpGetJSON(addb.drinks + '&start=0&pageSize=25');
    },

    componentWillUnmount: function () {
        window.removeEventListener('scroll', this.handleScroll);
    },

    handleScroll(){
        if ($(window).scrollTop() + $(window).height() > $(document).height() - 50) {
            this.handleNextPage();
            window.removeEventListener('scroll', this.handleScroll);
        }
    },

    // Eventhandler to load more drinks when pushing the button
    handleNextPage: function (e) {
        if (this.state.searchText === '') {
            var url = addb.drinks + '&start=' + this.state.counter + '&pageSize=25';
            this.httpGetJSON(url);
            this.setState({
                counter: this.state.counter + 25,
                loading: true
            })
        } else {
            var url = addb.quickSearch + this.state.searchText + '/?apiKey=' + addb.apiKey + '&start=' + this.state.searchCounter + '&pageSize=25';
            this.httpGetJSON(url);
            this.setState({
                searchCounter: this.state.searchCounter + 25,
                loading: true
            })
        }
    },

    setDisplayedResults(results){
        this.setState({
            displayedResults: results
        })
    },

    // FILTERS
    // Function for applying the filters from the dropdown menus
    applyFilters(){
        var drinks = this.state.unFilteredResults;
        // Run Drinks through active filters
        if (this.state.colorFilter.active) {
            drinks = this.filterByColor(this.state.colorFilter.color, drinks)
        }
        if (this.state.tasteFilter.active) {
            drinks = this.filterByTaste(this.state.tasteFilter.taste, drinks);
        }

        this.setState({
            displayedResults: drinks
        })
    },

    // function for applying the color filter
    setColorFilter(active, color){
        this.setState({
            colorFilter: {'active': active, color: color}
        }, function () {
            this.applyFilters();
        });
    },

    // Function for filtering the results
    filterByColor(color, drinks){
        return $.grep(drinks, function (v) {
            return v.color === color;
        });
    },

    // Function for applying the taste filter
    setTasteFilter(active, taste){
        this.setState({
            tasteFilter: {'active': active, taste: taste}
        }, function () {
            this.applyFilters();
        });
    },

    // Function for filtering the results
    filterByTaste(taste, drinks){
        var temp_drinks = [];
        for (var i = 0; i < drinks.length; i++) {
            for (var j = 0; j < drinks[i].tastes.length; j++) {
                if (taste === drinks[i].tastes[j].text) {
                    temp_drinks.push(drinks[i])
                }
            }
        }
        return temp_drinks;
    },

    // Render function that passes props down to MainContent.
    render: function () {
        return (
            <MainContent
                toggle={this.state.toggle}
                view={this.toggleview}
                onUpdateSearch={this.handleUpdateSearch}
                search={this.state.searchText}
                results={this.state.results}
                loading={this.state.loading}
                initialLoaded={this.state.initalLoaded}
                // RESULT TABLE
                // Filters
                applyFilters={this.applyFilters}
                setColorFilter={this.setColorFilter}
                setTasteFilter={this.setTasteFilter}
                // Sort
                setDisplayedResults={this.setDisplayedResults}
                counter={this.state.counter}
                displayedResults={this.state.displayedResults}
                onNextPage={this.handleNextPage}
            />
        )
    }
});

module.exports = MainContentContainer;
