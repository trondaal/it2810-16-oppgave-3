var $ = require('jquery');
var React = require('react');
var apiRoutes = require('../config/apiRoutes');
var FrontPage = require('../components/FrontPage');

var FrontPageContainer = React.createClass({
    // The inital state of this component
    getInitialState: function () {
        return {
            isLoggedIn: false
        }
    },

    // Gets called the first time this component renders.
    componentDidMount: function () {
        // Async API call for checking user login status.
        $.get(apiRoutes.users.status, function (status) {
            this.setState(() => {
                return {
                    isLoggedIn: status
                };
            });
        }.bind(this));
    },

    render: function () {
        return (
            <FrontPage
                isLoggedIn={this.state.isLoggedIn}
                child={this.props.children}/>
        )
    }
});

module.exports = FrontPageContainer;