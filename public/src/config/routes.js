var React = require('react');
var ReactRouter = require('react-router');
var Route = ReactRouter.Route;
var Router = ReactRouter.Router;
var IndexRoute = ReactRouter.IndexRoute;
var hashHistory = ReactRouter.hashHistory;
var UserContainer = require('../containers/UserContainer');
var FrontPageContainer = require('../containers/FrontPageContainer');
var MainContentContainer = require('../containers/MainContentContainer');

//routes handles the routing of the applictaion, so for example if the site goes to the url /user
//FrontPageContainer with the UserContainer as a child gets rendered.
var routes = (
	<Router history={hashHistory}>
		<Route path='/' component={FrontPageContainer} >
			<IndexRoute component={MainContentContainer} />
			<Route path="user" component={UserContainer} />
		</Route>
	</Router>
);

module.exports = routes;