module.exports = {
    'favorites' : {
        'find'      : './favorites',
        'add'       : './favorites/add',
        'delete'    : './favorites/delete',
        'isFavorite': './favorites/isFavorite'
    },
    'users'     : {
        'find'      : './users/user',
        'status'    : './users/status'
    },
    'histories' : {
        'find'      : './histories',
        'add'       : './histories/add',
        'delete'    : './histories/delete'
    }
};