// Config file for the API calls to shorten the links
module.exports = {
    'appID'         : '10849',
    'apiKey'        : '4619acb440ef42fb9e181f2748a539c6',
    'baseURL'       : 'http://addb.absolutdrinks.com',
    'drinks'        : 'http://addb.absolutdrinks.com/drinks/?apiKey=4619acb440ef42fb9e181f2748a539c6',
    'quickSearch'   : 'http://addb.absolutdrinks.com/quickSearch/drinks/',
    'assetURL' : {
        'drinks'        : 'http://assets.absolutdrinks.com/drinks/',
        'drinks_transp' : 'http://assets.absolutdrinks.com/drinks/transparent-background-white/'
    }
};
