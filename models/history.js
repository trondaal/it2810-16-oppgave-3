/**
 * Created by sondre on 13/10/2016.
 */
var pool = require('../db.js');

/**
 * Finds all the histories for a given user.
 * @param {String} user_id Unique identifier of the user.
 * @param {Function} callback Result and error of the query are arguments. Result is an array of favorite objects.
 */
exports.get_all_by_user = function (user_id, callback) {
    pool.getConnection(function (err, connection) {
        connection.query('SELECT * FROM histories WHERE user_id = ? ORDER BY date DESC', user_id, function (err, result) {
            if (err) return callback(err);
            callback(null, result.splice(0, 5));
            connection.release();
        })
    });
};

/**
 * Adds a new history to the database for a given user.
 * @param {String} user_id Unique identifier of the user.
 * @param {String} drink_id The drink to be logged.
 * @param {String} drink_name The name of the drink to be logged.
 * @param {Function} callback Result and error of the query are arguments.
 */
exports.add = function (user_id, drink_id, drink_name, callback) {
    var values = [user_id, drink_id, drink_name];
    pool.getConnection(function (err, connection) {
        connection.query('INSERT INTO histories (user_id, drink_id, drink_name) VALUES(?, ?, ?)', values, function (err, result) {
            if (err) return callback(err);
            callback(null, result);
            connection.release();
        });
    })
};

/**
 * Deletes a history from the database for a given user.
 * @param {String} user_id Unique identifier of the user.
 * @param {String} drink_id Unique identifier of the drink.
 * @param {Function} callback Result and error of the query are arguments.
 */
exports.delete = function (user_id, drink_id, callback) {
    var values = [user_id, drink_id];
    pool.getConnection(function (err, connection) {
        connection.query('DELETE FROM histories WHERE user_id = ? AND drink_id = ?', values, function (err, result) {
            if (err) return callback(err);
            callback(null, result);
            connection.release();
        })

    })
};