/**
 * Created by sondre on 13/10/2016.
 */
var pool = require('../db.js');

/**
 * Adds a new user to the database
 * @param {String} id Unique identifier
 * @param {String} token Access-token provided by the authentication service
 * @param {String} name Name of the user
 * @param {String} email The user's email
 * @param {Function} callback Result and error of the query are arguments.
 */
exports.create = function (id, token, name, email, callback) {
    var values = [id, token, name, email];
    pool.getConnection(function (err, connection) {
        connection.query('INSERT INTO users (user_id, token, name, email) VALUES(?, ?, ?, ?)', values, function (err, result) {
            if (err) return callback(err);
            callback(null, {"user_id" : id, "token" : token, "name" : name, "email" : email});
            connection.release();
        })
    })
};

/**
 * Finds a user in the database by user id.
 * @param {String} id Unique identifier of the user we want to find.
 * @param {Function} callback Result and error of the query are arguments. Result is the user if exist, false otherwise.
 */
exports.find = function (id, callback) {
    pool.getConnection(function (err, connection) {
        connection.query('SELECT * FROM users WHERE user_id = ?', id, function (err, result) {
            if (err) return callback(err);
            if (result.length == 0) return callback(null, false);
            callback(null, JSON.parse(JSON.stringify(result[0])));
            connection.release();
        })
    })
};

/**
 * Finds all the users in the database (used for debugging)
 * @param {Function} callback Result and error of the query are arguments. Result is an array of user objects.
 */
exports.get_all = function (callback) {
    pool.getConnection(function (err, connection) {
        connection.query('SELECT * FROM users', function (err, result) {
            if (err) return callback(err);
            callback(null, result);
            connection.release();
        })
    })
};

/**
 * Validates that a user is signed in and authenticated.
 * @param {Object} req Request object representing a HTTP request. The request contains user data.
 * @param {Object} res Response object representing a HTTP response.
 * @param {Function} next A function that is called if the user is authenticated.
 */
exports.isLoggedIn = function (req, res, next) {
    // If user is authenticated in the session, carry on
    if (req.isAuthenticated()) {
        console.log(
            'Request from authenticated user:' + '\n' +
            'User = ' + req.user.user_id + '\n' +
            'Request = ' + req.originalUrl + '\n' +
            'SessionID = ' + req.sessionID + '\n' +
            '---');
        return next();
    }
    // If they aren't redirect them to the home page
    res.send("Permission denied");
};



