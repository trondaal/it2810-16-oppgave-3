/**
 * Created by sondre on 13/10/2016.
 */
var pool = require('../db.js');

/**
 * Finds all the favorites for a given user.
 * @param {String} user_id Unique identifier of the user.
 * @param {Function} callback Result and error of the query are arguments. Result is an array of favorite objects.
 */
exports.get_all_by_user = function (user_id, callback) {
    pool.getConnection(function (err, connection) {
        connection.query('SELECT drink_id FROM favorites WHERE user_id = ?', user_id, function (err, result) {
            if (err) return callback(err);
            callback(null, JSON.parse(JSON.stringify(result)));
            connection.release();
        })
    })
};

/**
 * Checks if the given drink is a favorite of the given user.
 * @param {String} user_id Unique identifier of the user.
 * @param {String} drink_id Unique identifier of the drink.
 * @param {Function} callback Result and error of the query are arguments. Result is an array of favorite objects.
 */
exports.is_favorited_by_user = function (user_id, drink_id, callback) {
    var values = [user_id, drink_id];
    pool.getConnection(function (err, connection) {
        connection.query('SELECT * FROM favorites WHERE user_id = ? AND drink_id = ?', values, function (err, result) {
            if (err) return callback(err);
            callback(null, result.length > 0);
            connection.release();
        })
    })
};

/**
 * Adds a new favorite to the database for a given user.
 * @param {String} user_id Unique identifier of the user.
 * @param {String} drink_id Unique identifier of the drink.
 * @param {Function} callback Result and error of the query are arguments.
 */
exports.add = function (user_id, drink_id, callback) {
    var values = [user_id, drink_id];
    pool.getConnection(function (err, connection) {
        connection.query('INSERT INTO favorites (user_id, drink_id) VALUES(?, ?)', values, function (err, result) {
            if (err) return callback(err);
            callback(null, result);
            connection.release();
        });
    })
};

/**
 * Deletes a favorite from the database for a given user.
 * @param {String} user_id Unique identifier of the user.
 * @param {String} drink_id Unique identifier of the drink to be removed from favorites.
 * @param {Function} callback Result and error of the query are arguments.
 */
exports.delete = function (user_id, drink_id, callback) {
    var values = [user_id, drink_id];
    pool.getConnection(function (err, connection) {
        connection.query('DELETE FROM favorites WHERE user_id = ? AND drink_id = ?', values, function (err, result) {
            if (err) return callback(err);
            callback(null, result);
            connection.release();
        })
    })
};
