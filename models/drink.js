var request = require('request');
var addb = require('../config/addb');


exports.get_drink = function (drink_id, callback) {
    request(addb.baseURL + '/drinks/' + 'golden-gleam' + '/?apikey=' + addb.apiKey,
        function (err, response, body) {
            if (err) callback(null, err);
            if (response.statusCode == 200) {
                callback(null, JSON.parse(body));
            }
        })
};

exports.get_all_drinks = function (drink_ids, callback) {
    var drinks = [];
    var numRunningQueries = 0;
    drink_ids.forEach(function (drink_id) {
        ++numRunningQueries;
        request(addb.baseURL + '/drinks/' + drink_id + '/?apikey=' + addb.apiKey,
            function (err, response, body) {
                if (err) callback(null, err);
                if (response.statusCode == 200) {
                    drinks.push(JSON.parse(body).result[0]);
                    --numRunningQueries;
                    if (numRunningQueries === 0) {
                        callback(null, drinks);
                    }
                }
            });
    })
};