var Main = React.createClass({

	render: function(){
		return(
			<div className="main-content">
				<Navbar />
				<MainContent />
			</div>
		);
	}
});

var Navbar = React.createClass({
	render: function(){
		return(
			<nav className="navbar navbar-inverse navbar-fixed-top">
		      <div className="container">
		        <div className="navbar-header">
		          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		            <span className="sr-only">Toggle navigation</span>
		            <span className="icon-bar"></span>
		            <span className="icon-bar"></span>
		            <span className="icon-bar"></span>
		          </button>
		          <a className="navbar-brand" href="#">Drinks</a>
		        </div>
		        <div id="navbar" className="navbar-collapse collapse">
		          <form className="navbar-form navbar-right">
		            <div className="form-group">
		              <input type="text" placeholder="Email" className="form-control" />
		            </div>
		            <div className="form-group">
		              <input type="password" placeholder="Password" className="form-control" />
		            </div>
		            <button type="submit" className="btn btn-success">Sign in</button>
		          </form>
		        </div>
		      </div>
		    </nav>
		);
	}
});

var MainContent = React.createClass({
	render: function(){
		return(
			<div className="jumbotron">
				<div className="container">
					<h1>Search for Drinks</h1>
					<p>Here you can search for drinks</p>
					<input type="text" placeholder="drinks/name" className="form-control" />
				</div>
			</div>
		);
	}
})


ReactDOM.render(
	<Main url="/api/comments"/>,
	document.getElementById('content')
);