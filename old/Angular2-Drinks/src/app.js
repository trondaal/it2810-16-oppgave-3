System.register(['@angular/core', '@angular/platform-browser', './tabs', './tab'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, platform_browser_1, tabs_1, tab_1;
    var App, AppModule;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (platform_browser_1_1) {
                platform_browser_1 = platform_browser_1_1;
            },
            function (tabs_1_1) {
                tabs_1 = tabs_1_1;
            },
            function (tab_1_1) {
                tab_1 = tab_1_1;
            }],
        execute: function() {
            App = (function () {
                function App() {
                    this.name = 'Angular2';
                }
                App = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        template: "\n    <tabs>\n      \n      <tab  tabTitle=\"Home\">\n        <img class=\"imageLeft\" src=\"../img/greatGatsby.jpg\" alt=\"pic: Rasmus \" height=auto width=50%/>\n        <article>\n          <p> You know those nights......<br/>\n          nights that become day, nights that become shared memories,<br/>\n          nights that open you up to connect and live shared memories. </p>\n        </article> \n      </tab>\n      <tab  tabTitle=\"Drinks\">\n        <a href=\"#\" > <img class=\"imageLeft\" src=\"../img/cosmo.jpg\" alt=\"pic: Cosmopolitan \"/> </a>\n      \n        <a href=\"#\" > <img class=\"imageCenter\" src=\"../img/yellowDrink.jpg\" alt=\"pic: Blue drink \"/> </a>\n       \n        <a href=\"#\" > <img class=\"imageRight\"  src=\"../img/blueDrink.jpg\" alt=\"pic: Rasmus \"/> </a>\n        \n      </tab>\n      <tab  tabTitle=\"Search\">Sometime in the near future <br/> it might be able to search for drinks here!  </tab>\n      <tab  tabTitle=\"About us\">We are group 16!</tab>\n    \n    </tabs>\n  "
                    }), 
                    __metadata('design:paramtypes', [])
                ], App);
                return App;
            }());
            AppModule = (function () {
                function AppModule() {
                }
                AppModule = __decorate([
                    core_1.NgModule({
                        imports: [platform_browser_1.BrowserModule],
                        declarations: [App, tabs_1.Tabs, tab_1.Tab],
                        bootstrap: [App]
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppModule);
                return AppModule;
            }());
            exports_1("AppModule", AppModule);
        }
    }
});
//# sourceMappingURL=app.js.map