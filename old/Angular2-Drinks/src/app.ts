//our root app component
import {Component, NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'

import {Tabs} from './tabs';
import {Tab} from './tab';

@Component({
  selector: 'my-app',
  template: `
    <tabs>
      
      <tab  tabTitle="Home">
        <img class="imageLeft" src="../img/greatGatsby.jpg" alt="pic: Rasmus " height=auto width=50%/>
        <article>
          <p> You know those nights......<br/>
          nights that become day, nights that become shared memories,<br/>
          nights that open you up to connect and live shared memories. </p>
        </article> 
      </tab>
      <tab  tabTitle="Drinks">
        <a href="#" > <img class="imageLeft" src="../img/cosmo.jpg" alt="pic: Cosmopolitan "/> </a>
      
        <a href="#" > <img class="imageCenter" src="../img/yellowDrink.jpg" alt="pic: Blue drink "/> </a>
       
        <a href="#" > <img class="imageRight"  src="../img/blueDrink.jpg" alt="pic: Rasmus "/> </a>
        
      </tab>
      <tab  tabTitle="Search">Sometime in the near future <br/> it might be able to search for drinks here!  </tab>
      <tab  tabTitle="About us">We are group 16!</tab>
    
    </tabs>
  `
})
class App {
  constructor() {
    this.name = 'Angular2'
  }
}

@NgModule({
  imports: [ BrowserModule ],
  declarations: [ App, Tabs, Tab ],
  bootstrap: [ App ]
})
export class AppModule {}



