import { Component, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { Tab } from './tab';

@Component({
  selector: 'tabs',
  template:`
    <h1>Drinks</h1>
    <form class="form" action="demo_form.asp">
      <input class ="inputfield" name="searchbox" onfocus="if (this.value=='Username') this.value = ''" type="text" value="Username"> 
      <input class ="inputfield" name="searchbox" onfocus="if (this.value=='Password') this.value = ''" type="text" value="Password">
    <input class="submit" type="submit" value="Login"/>
    </form>
    <ul class="nav">
      <li class="navLi" *ngFor="let tab of tabs" (click)="selectTab(tab)" [class.active]="tab.active">
        <a [style.font-size]="getStyleBold(tab)" [style.text-decoration]="getStyleUnderline(tab)" [style.color]="getStyleColor(tab)" href="#">{{tab.title}}</a>
      </li>
    </ul>
    <ng-content></ng-content>
  `
})
export class Tabs implements AfterContentInit {
  
  @ContentChildren(Tab) tabs: QueryList<Tab>;
  
  // contentChildren are set
  ngAfterContentInit() {
    // get all active tabs
    let activeTabs = this.tabs.filter((tab)=>tab.active);
    
    // if there is no active tab set, activate the first
    if(activeTabs.length === 0) {
      this.selectTab(this.tabs.first);
    }
  }
  
  selectTab(tab: Tab){
    // deactivate all tabs
    this.tabs.toArray().forEach(tab => tab.active = false);
    
    // activate the tab the user has clicked on.
    tab.active = true;


  }

  getStyleColor(tab: Tab) {
    if (tab.active){
      return "#ffff66"
    }

    
    // activate the tab the user has clicked on.
  }

  getStyleUnderline(tab: Tab) {
    if (tab.active){
      return "underline"
    }


    
    // activate the tab the user has clicked on.
  }

  getStyleBold(tab: Tab) {
    if (tab.active){
      return "23px"
    }


    
    // activate the tab the user has clicked on.
  }




}
