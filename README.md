# Drinks
Project in course IT2810 at NTNU. Group 16.
Application running at http://it2810-16.idi.ntnu.no/

### Introduction ###

Drinks is a web application written in React, that lets you browse a wide variety of drinks. You can get their recipe and other useful information, such as mix difficulty and user ratings. By creating a profile, you can save your favorite drinks for later use. The application uses the Absolut Drinks Database, which is an API for drinks and other related assets.

### Installation ###
The application requires [Node.js](https://nodejs.org/) v6, [npm package manager](https://www.npmjs.com/) and [MySQL Community Server](http://dev.mysql.com/downloads/) in order to run.

Clone the repository
```sh
$ git clone https://<username>@bitbucket.org/trondaal/it2810-16-oppgave-3.git
```
Import and set up the MySQL database 

```sh
$ cd it2810-16-oppgave-3
$ mysql -u <username> -p
$ create database it2810;
$ use it2810;
$ \. it2810.sql
```

Replace the example database settings in  ```db.js ```  with your database information.

Install the dependencies, build the project and start the server.
```sh
$ npm install
$ npm run build
$ npm start
```
The application should now be running at http://localhost:3000

#### Project Structure ####

##### Server ####
  
```sh
src
│   package.json
│   db.js
│   server.js
│  
├───config
│       addb.js
│       auth.js
│       passport.js
│
├───controllers
│       favorites.js
│       histories.js
│       routes.js
│       users.js
│
├───models
│       drink.js
│       favorite.js
│       history.js
│       user.js
│
└───public*
```

##### Client #####

```sh
│   index.html
│
├───build
│       app.js
│
├───css
│       main.css
│
├───images
│   │   load.svg
│   │
│   └───google
│           btn_google_signin.png
│           btn_google_signin_2x.png
│
└───src
    │   index.js
    │
    ├───components
    │   │   FrontPage.js
    │   │   MainContent.js
    │   │   NavBar.js
    │   │   Result.js
    │   │
    │   ├───drinkgrid
    │   │       DrinkGrid.js
    │   │       DrinkGridColumn.js
    │   │
    │   ├───modal
    │   │       DrinkModal.js
    │   │       Tags.js
    │   │
    │   ├───table
    │   │       DrinkTable.js
    │   │       DrinkTableRow.js
    │   │       Filter.js
    │   │
    │   └───user
    │           DrinkListItem.js
    │           FavoriteList.js
    │           HistoryList.js
    │           User.js
    │
    ├───config
    │       addb.js
    │       apiRoutes.js
    │       routes.js
    │
    └───containers
            FrontPageContainer.js
            MainContentContainer.js
            UserContainer.js
```
#### Overview of the React components ####
![component_overview.png](https://bitbucket.org/repo/RErpzy/images/1157859454-component_overview.png)

###Client structure###

* Components
* Containers

The React components are for the most part divided into stateful components and functional stateless components. This allows the components to be more focused so that a component either deals with its state and functions that alter that state or renders. Take the MainContent.js component, it only consists of a function that returns what should be rendered. MainContentContainer.js on the other hand only renders MainContent, but handles state and passes the values of the state and functions to alter the state down to MainContent.js as props.
### Tech

Drinks uses a number of open source libraries and frameworks:

##### Server #####

* [Express](http://expressjs.com/) - Minimal and flexible Node.js web application framework.
* [Passport](http://passportjs.org/) -  Authentication middleware for Node.js (used with the Google authentication strategy)
* [MySQL for Node](https://github.com/mysqljs/mysql) - The MySql protocol for Node.js.

##### Client ######

* [React](https://facebook.github.io/react/) - JavaScript library used for building the user interface.
* [jQuery](https://jquery.com/) - JavaScript library (Mostly used for Ajax).
* [React-bootstrap](https://react-bootstrap.github.io/) - The front-end framework Bootstrap, rebuilt for React.
* [React-router](https://github.com/ReactTraining/react-router) - Keeps the UI in sync with the URL

[Browserify](http://browserify.org/) - Used for building and bundling the project dependencies.