/**
 * Created by sondre on 20/10/2016.
 */
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
// Load the user model
var User = require('../models/user');
// Load auth configuration
var configAuth = require('./auth');

module.exports = function (passport) {

    // Serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.user_id);
    });

    // Deserialize the user
    passport.deserializeUser(function (id, done) {
        User.find(id, function (err, user) {
            done(err, user);
        });
    });

    // GOOGLE CONFIGURATION
    passport.use(new GoogleStrategy({

            clientID: configAuth.googleAuth.clientID,
            clientSecret: configAuth.googleAuth.clientSecret,
            callbackURL: configAuth.googleAuth.callbackURL
        },
        function (token, refreshToken, profile, done) {
            process.nextTick(function () {
                // Look up the user in the database
                User.find(profile.id, function (err, user) {
                    if (err)
                        return done(err);
                    if (user)
                    // If a user is found, proceed to login
                        return done(null, user);
                    else {
                        // If the user is not already in our database, create a new user
                        User.create(profile.id, token, profile.displayName, profile.emails[0].value, function (err, user) {
                            if (err)
                                throw err;
                            return done(null, user)
                        });
                    }
                });
            });
        }));

};