/**
 * Created by sondre on 13/10/2016.
 */
var express = require('express');
var router = express.Router();
var user = require('../models/user');

// GET-request for retrieving all users (used for debugging). User must be logged in.
router.get('/', user.isLoggedIn, function (req, res) {
    user.get_all(function (err, result) {
        res.json(result);
    });
});

// GET-request for retrieving user information
router.get('/user', user.isLoggedIn, function (req, res) {
    res.json(req.user)
});

// GET-request for checking login status
router.get('/status', function (req, res) {
    res.send(req.isAuthenticated());
});

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});


module.exports = router;
