/**
 * Created by sondre on 20/10/2016.
 */

/**
 * Configures all the routes and initializes Passport which is used for authentication.
 * Authentication needs to be configured before defining the routes.
 */
module.exports = function (app, passport) {
    var user = require('../models/user');
    var session = require('express-session');
    var users = require('./users');
    var favorites = require('./favorites');
    var histories = require('./histories');
    
    app.use(session({secret: 'it2810-16secretsession'}));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use('/users', users);
    app.use('/favorites', favorites);
    app.use('/histories', histories);

    /* OAuth 2.0 authentication and callback routes */

    // Google
    app.get('/auth/google', passport.authenticate('google', {scope: ['profile', 'email']}));
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/',
            failureRedirect: '/'
        }));
};


