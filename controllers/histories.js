/**
 * Created by sondre on 13/10/2016.
 */
var express = require('express');
var router = express.Router();
var history = require('../models/history');
var user = require('../models/user');
var drink = require('../models/drink');

// GET-request for retrieving all histories for a logged in user.
router.get('/', user.isLoggedIn, function (req, res) {
    var user_id = req.user.user_id;
    history.get_all_by_user(user_id, function (err, histories) {
        res.json(histories);
        })
});

// POST-request for adding a history for a logged in user.
router.post('/add', user.isLoggedIn, function (req, res) {
    var user_id = req.user.user_id;
    var drink_id = req.body.drink.id;
    var drink_name = req.body.drink.name;
    history.add(user_id, drink_id, drink_name, function (err, result) {
        res.json(result); // Replace with redirect
    });
});

// POST-request for deleting a history for a logged in user.
router.post('/delete', user.isLoggedIn, function (req, res) {
    var user_id = req.user.user_id;
    var drink_id = req.body.drink.id;
    history.delete(user_id, drink_id, function (err, result) {
        res.json(result); // Replace with redirect
    });
});

module.exports = router;