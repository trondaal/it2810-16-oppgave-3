/**
 * Created by sondre on 13/10/2016.
 */
var express = require('express');
var router = express.Router();
var user = require('../models/user');
var drink = require('../models/drink');
var favorite = require('../models/favorite');

// GET-request for retrieving all favorites for a logged in user.
router.get('/ids', user.isLoggedIn, function (req, res) {
    var user_id = req.user.user_id;
    favorite.get_all_by_user(user_id, function (err, favorites) {
        res.json(favorites)
    });
});

// GET-request for retrieving all favorites for a logged in user.
router.get('/', user.isLoggedIn, function (req, res) {
    var user_id = req.user.user_id;
    favorite.get_all_by_user(user_id, function (err, drink_ids) {
        if (drink_ids.length > 0){
            var temp_drink_ids = [];
            for (var i = 0; i < drink_ids.length; i++) {
                temp_drink_ids.push(drink_ids[i].drink_id)
            }
            drink.get_all_drinks(temp_drink_ids, function (err, drinks) {
                res.json(drinks)
            })
        } else {
            res.json([])
        }
    });
});

// POST-request for checking if the given drink is a favorite of the user
router.post('/isFavorite', user.isLoggedIn, function (req, res) {
    var user_id = req.user.user_id;
    var drink_id = req.body.drink.id;
    favorite.is_favorited_by_user(user_id, drink_id, function(err, result){
        res.json(result);
    })
});

// POST-request for adding a favorite for a logged in user.
router.post('/add', user.isLoggedIn, function (req, res) {
    var user_id = req.user.user_id;
    var drink_id = req.body.drink.id;
    favorite.add(user_id, drink_id, function (err, result) {
        res.json(result); // Replace with redirect
    });
});

// POST-request for deleting a favorite for a logged in user.
router.post('/delete', user.isLoggedIn, function (req, res) {
    var user_id = req.user.user_id;
    var drink_id = req.body.drink.id;
    favorite.delete(user_id, drink_id, function (err, result) {
        res.json(result); // Replace with redirect
    });
});

module.exports = router;