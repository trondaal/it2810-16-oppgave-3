/**
 * Created by sondre on 13/10/2016.
 */
var mysql = require('mysql');

/**
 * Establishes a connection to the MySQL server.
 * @returns {Connection} A Connection object used for database queries.
 */

var pool  = mysql.createPool({
    host     : 'localhost',
    database : 'it2810',
    user     : 'root',
    password : ''
});

module.exports = pool;
