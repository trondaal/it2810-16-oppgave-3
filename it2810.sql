-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11. Nov, 2016 19:19 p.m.
-- Server-versjon: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `it2810`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `favorites`
--

CREATE TABLE `favorites` (
  `drink_id` varchar(250) NOT NULL,
  `user_id` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `histories`
--

CREATE TABLE `histories` (
  `drink_id` varchar(250) NOT NULL,
  `user_id` varchar(250) NOT NULL,
  `drink_name` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `histories`
--

INSERT INTO `histories` (`drink_id`, `user_id`, `drink_name`, `date`) VALUES
('skylab', '113279684670387177567', 'Skylab', '2016-11-11 18:18:43'),
('pepparmint-frape', '113279684670387177567', 'Pepparmint Frapé', '2016-11-11 18:18:49');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `users`
--

CREATE TABLE `users` (
  `user_id` varchar(250) NOT NULL,
  `token` text NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `users`
--

INSERT INTO `users` (`user_id`, `token`, `name`, `email`) VALUES
('113279684670387177567', 'ya29.Ci-TA-pOju-9Nh092icHzccBFOoYZEiMlDtjEli-PdWmTLzpK3NJOI7010DxnPOqdA', 'Sondre H', 'sondrehjetland@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`drink_id`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`user_id`,`date`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
