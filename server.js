/**
 * Created by sondre on 13/10/2016.
 */
var express = require('express');
var db = require('./db');
var app = express();
var path = require('path');
var user = require('./models/user');
var favorite = require('./models/favorite');
var history = require('./models/history');
var passport = require('passport');
var passConf = require('./config/passport')(passport);
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
var routes = require('./controllers/routes')(app, passport);
app.use('/', express.static(path.join(__dirname, 'public')));

// Server settings
var PORT = 3000;

app.listen(PORT, function () {
    console.log('Listening on port 3000...')
});

